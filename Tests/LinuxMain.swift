import XCTest

import HLBaseAdapterKitTests

var tests = [XCTestCaseEntry]()
tests += HLBaseAdapterKitTests.allTests()
XCTMain(tests)
