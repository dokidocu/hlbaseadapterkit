//
//  RestConfiguration.swift
//  HLNetKit
//
//  Created by Henri LA on 18.10.19.
//

import Foundation

public struct RestConfiguration {
  
  public let scheme: String
  public let host: String
  public let version: String?
  
  public init(scheme: String, host: String, version: String?){
    self.scheme = scheme
    self.host = host
    self.version = version
  }
  
  public func url(path: String, queryItems:[URLQueryItem]) -> URL? {
    var urlComponents = URLComponents()
    
    urlComponents.scheme = scheme
    urlComponents.host = host
    if let version = version {
      urlComponents.path = "/\(version)" + path
    } else {
      urlComponents.path = path
    }
    urlComponents.queryItems = queryItems
    
    return urlComponents.url
  }
  
}
